Ejecutar el Jupyter Notebook
=============================


*  Como primer paso debemos instalar el entorno Anaconda (más información en el siguiente link: [https://www.anaconda.com/distribution/#download-section](https://www.anaconda.com/distribution/#download-section)
*  Luego debemos clonar el respositorio en la carpeta que deseemos con el siguiente comando: 
`git clone git@gitlab.com:magual89/cyt.git`

*  Una vez ejecutado Anaconda el notebook `tp1.ipynb` puede ser cargado clickeando en el icono *Jupyter Notebook*. 

Esto lanzará una nueva venta o pestaña con un dashboard, y un panel de control que nos permitirá seleccionar el notebook que deseamos abrir simplemente clickeandolo.

